import React from 'react';
import { LineChart } from '../components/LineChart';
import { DeviceList } from  '../components/DeviceList';

interface Props {}
interface State {
    deviceUUID: string | null;
    endTime: number | null;
    windowTime: number | null;
    numWindows: number | null;
    devices: Array<any>;
    timestamps: Array<any>;
    bytesTS: Array<any>;
    bytesFS: Array<any>;
}

export class Bandwidths extends React.PureComponent<Props, State> {
    constructor(props: Props) {
        super(props);

        const searchParams = new URLSearchParams(window.location.search);

        this.state = {
            deviceUUID: searchParams.get('device_uuid'),
            endTime: Number(searchParams.get('end_time')),
            windowTime: Number(searchParams.get('window_time')),
            numWindows: Number(searchParams.get('num_windows')),
            devices: [],
            timestamps: [],
            bytesFS: [],
            bytesTS: [],
        };

        this.handleBandwidthFetch = this.handleBandwidthFetch.bind(this);
        this.handleDeviceClick = this.handleDeviceClick.bind(this);
    }

    handleBandwidthFetch() {
        const {
            deviceUUID,
            endTime,
            windowTime,
            numWindows,
        } = this.state;

        // TODO: remove hardcoded endpoint
        fetch(`http://127.0.0.1:5000/bandwidths?device_uuid=${deviceUUID}&end_time=${endTime}&window_time=${windowTime}&num_windows=${numWindows}`)
            .then(res => res.json())
            .then(res => {
                debugger;
                this.setState({
                    timestamps: res.timestamps,
                    bytesFS: res.bytes_fs,
                    bytesTS: res.bytes_ts,
                });
            })
            .catch(err => console.log(err));
    }

    handleDeviceClick(device_id: string) {
        this.setState({
            deviceUUID: device_id,
        }, () => {
            this.handleBandwidthFetch();
        })
    }

    componentWillMount() {
        const {
            deviceUUID,
        } = this.state;

        if (deviceUUID !== null && deviceUUID !== undefined) {
            this.handleBandwidthFetch();
        }
 
        fetch(`http://127.0.0.1:5000/devices`)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    devices: res.devices,
                })
            })
            .catch(err => {
                debugger;
                console.log(err)
            });
    }

    render() {
        const {
            timestamps,
            bytesFS,
            bytesTS,
            devices,
        } = this.state;

        return (
            <div style={{ width: '100%', height: '100vh' }}>
                <div style={{ height: '50vh', overflow: 'auto' }}>
                    <DeviceList devices={devices} onDeviceClick={this.handleDeviceClick} /> 
                </div>
                <div>
                    <LineChart timestamps={timestamps} bytesFS={bytesFS} bytesTS={bytesTS}/>
                </div>
            </div>
        );
    }
}

