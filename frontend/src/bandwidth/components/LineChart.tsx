import React from "react";
import Chart from 'chart.js';

interface Props {
    timestamps: Array<any>,
    bytesTS: Array<any>,
    bytesFS: Array<any>,
}
interface State {}

export class LineChart extends React.PureComponent<Props, State> {
    chartRef: any;

    constructor(props: Props) {
        super(props);

        this.chartRef = React.createRef();

        this.buildChart = this.buildChart.bind(this);
    }

    buildChart() {
        const chartContext = this.chartRef.current.getContext("2d");

        const {
            timestamps,
            bytesFS,
            bytesTS,
        } = this.props;

        new Chart(chartContext, {
            type: "line",
            data: {
                labels: timestamps,
                datasets: [
                    {
                        label: 'bytes to server',
                        data: bytesTS,
                        fill: false,
                        borderColor: '#ff3333',
                    },
                    {
                        label: 'bytes from server',
                        data: bytesFS,
                        fill: false,
                        borderColor: '#3333ff',
                    },
                ]
            },
        });
    }

    componentDidMount() {
        this.buildChart();
    }

    componentDidUpdate() {
        this.buildChart();
    }

    render() {
        return (
            <div>
                <canvas
                    id="chart"
                    ref={this.chartRef}
                />
            </div>
        );
    }
}
