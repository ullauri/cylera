import React from 'react';
import ReactDOM from 'react-dom';

import { Bandwidths } from './bandwidth/containers/Bandwidths';


function App() {
  return (
    <Bandwidths />
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

