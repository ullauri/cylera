import datetime
import pandas as pd

from flask import Flask, request
from flask_cors import CORS

from .bandwidths import BANDWIDTHS
from .devices import DEVICES

app = Flask(__name__)
CORS(app)

@app.route('/devices')
def devices():
    return {'devices': DEVICES}

# preprocess BANDWIDTHS to a manageable timestamp
for bandwidth in BANDWIDTHS:
    bandwidth['timestamp'] = datetime.datetime.fromtimestamp(bandwidth['timestamp'])
BANDWIDTHS = pd.DataFrame(BANDWIDTHS).sort_values('timestamp')

# offset to start window from
WINDOW_BASE = BANDWIDTHS['timestamp'][0].second

@app.route('/bandwidths')
def bandwidths():
    device_uuid = request.args['device_uuid']

    end_time = request.args.get('end_time') 
    if end_time is None or end_time == '0':
        end_time = pd.to_datetime('today')
    elif (isinstance(end_time, str)):
        end_time = datetime.datetime.fromtimestamp(int(end_time))

    window_time = request.args.get('window_time')
    if window_time is None or window_time == '0':
        window_time = 60

    num_windows = request.args.get('num_windows')
    if num_windows is None or num_windows == '0':
        num_windows = 10

    import pdb; pdb.set_trace()
    df = BANDWIDTHS[BANDWIDTHS.device_id == device_uuid]
    df = df[df.timestamp <= end_time]
    df = df.groupby(pd.Grouper(key='timestamp', freq='{}s'.format(window_time), base=WINDOW_BASE)).sum()
    timestamps = list(map(lambda t: str(t), df.index))[:num_windows]
    bytes_ts = list(map(lambda l: int(l), df['bytes_ts'].values))[:num_windows]
    bytes_fs = list(map(lambda l: int(l), df['bytes_fs'].values))[:num_windows]

    return {'timestamps': timestamps, 'bytes_ts': bytes_ts, 'bytes_fs': bytes_fs}


