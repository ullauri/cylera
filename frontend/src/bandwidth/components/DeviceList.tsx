import React from 'react';

// TODO: interface for below
/*
class: "Medical"
device_id: "7f47ccf9-582d-42b8-88bd-052a1d236839"
location: "Satellite Clinic"
model: "Alaris GW Volumetric Pump"
organization: "NYU"
type: "Infusion Pump"
*/

interface Props {
  devices: Array<any>;
  onDeviceClick: any;
}

export function DeviceList(props: Props) {
  return (
    <div>
        {props.devices.map(device => (
            <div
                style={{ border: '1px solid black', margin: '0 0 10px 0'}}
                onClick={e => props.onDeviceClick(device.device_id)}
            >
                <p>Model: {device.model}</p>
                <p>Type: {device.type}</p>
                <p>Location: {device.location}</p>
            </div>
        ))}
    </div>
  )
}
