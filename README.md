# Instructions

`git clone https://ullauri@bitbucket.org/ullauri/cylera.git`

`cd cylera`

## Server Side
if `pipenv` in not installed:
`pip install pipenv`

`pipenv shell --three`

`pipenv install`

`FLASK_APP=cylera/api.py flask run`

## Client Side
`cd frontend`

`npm i`

`npm start`

`http://localhost:3000/?device_uuid={}&end_time={}&window_time={}&num_windows={}`
